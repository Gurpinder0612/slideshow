
$(document).ready(function(){
  $("button").click(function(){
     var listvalue= $("#myInput").val();//assigining value of myinput(text box) id to variable listvalue
    $('ol').append('<li>' + listvalue +"<button class='done'>"+
    "Done</button> " + " <button class='remove'>"+
    "Remove</button></li>");//concatinate list item with buttons
     $("#myInput").val('');//to make list box empty
     
  });

  $(document).on('click', ".done", function(e){
    e.preventDefault();

    let listItem1 = this;

    $(listItem1).parent().toggleClass("strike");//CSS text decoration property (line-through)
    
    if ($(listItem1).text() === "Done") 
        $(listItem1).text('Undo'); //changing button text to undo
    
    else 
        $(listItem1).text('Done'); //changing button text to done   
  });

 
   $('ol').on('click','.remove',function() {
    $(this).parent('li').fadeOut(200);//remove or fade list item

});
 });


