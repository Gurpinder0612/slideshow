# Gurpinder Kaur           student Id:8715526

# Todo List
## General Instructions
### This is the drop down list where you can add or remove your list items.
### By default list is empty. To check its working you need to add any data or information in it.
### To add , you have to write your input in textbox then you have to click + button right side of the textbox.
### below the textbox, your ouput will be visible to you , now you can remove or undo your last step or input.

## installation Instructions
### no need to have any local server or other software to run this file, because it is a html file which can directly execute on web browser.
### No need to have any database management tool, as this application is only working on the basis of java script dom functions.

## Licence information
### Application is using MIT License.
### why MIT?
### As a permissive license, it puts only very limited restriction on reuse and has, therefore, high license compatibility.
### MIT licensed software can be re-licensed as GPL software, and integrated with other GPL software, but not the other way around. The MIT license also permits reuse within proprietary software, provided that either all copies of the licensed software include a copy of the MIT License terms and the copyright notice, or the software is re-licensed to remove this requirement.